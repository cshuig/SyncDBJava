<span style="white-space:pre"></span>这一篇通过JAVA读取Excel，将Excel表结构同步到数据库中，并生成java实体类。与上一篇类似，这里只介绍类说明，具体实现，可查看源码。<br />
<br />
流程：<br />
1：初始化数据库--&gt;创建目标库--&gt;创建元数据表<br />
2：读取Excel--&gt;封装为JavaBean--&gt;添加不存在的表--&gt;修改变动的表--&gt;添加不存在的字段--&gt;修改变动的字段<br />
3：读取Excel--&gt;封装为JavaBean--&gt;生成相对应的java实体<br />
<br />
<strong><span style="font-size:12px">同样，先介绍类结构:</span></strong><br />
<img src="http://img.blog.csdn.net/20130821224643265?watermark/2/text/aHR0cDovL2Jsb2cuY3Nkbi5uZXQvaHphY2NwMw==/font/5a6L5L2T/fontsize/400/fill/I0JBQkFCMA==/dissolve/70/gravity/SouthEast" alt="" /><br />
Column.java和Table.java：列和表的实体<br />
ConConfig.java：连接类的实体，包含用户名，密码和数据库地址<br />
<br />
ConnectionHelper.java：数据库连接操作工具类，包括测试连接、打开、关闭连接<br />
ExcelHelper.java：excel表操作工具类，用于将Excel表封装成javabean<br />
MssqlDBHelper.java：对应sqlserver底层操作类，包括建库、建表、同步表及将数据库中tableMate和columnMate封装成javabean<br />
<br />
SyncDbBiz.java 业务层，包括同步数据库和生成sql.<br />
MainWindow.java 操作界面.<br />
<br />
JavaCodeHelper.java：Java类操作的工具类，包括创建类、添加字段、添加方法、保存成.java文件等操作。<br />
TypeMapping.java：数据库类型与Java数据类型映射工具类<br />
EntityBuilderBiz.java：java实体生成业层,主要生成java类<br />
<br />
<strong>Excel数据结构:</strong><br />
<img src="http://img.blog.csdn.net/20130821224935203?watermark/2/text/aHR0cDovL2Jsb2cuY3Nkbi5uZXQvaHphY2NwMw==/font/5a6L5L2T/fontsize/400/fill/I0JBQkFCMA==/dissolve/70/gravity/SouthEast" alt="" /><br />
<br />
<strong>界面：</strong><br />
<img src="http://img.blog.csdn.net/20130821225051265?watermark/2/text/aHR0cDovL2Jsb2cuY3Nkbi5uZXQvaHphY2NwMw==/font/5a6L5L2T/fontsize/400/fill/I0JBQkFCMA==/dissolve/70/gravity/SouthEast" alt="" /><br />
<span style="color:#ff0000">如果择选文件慢，在jvm中添加-Djxl.nogc=true.</span><br />
<br />
<strong>生成的数据库结构：</strong><br />
<img src="http://img.blog.csdn.net/20130821225232671?watermark/2/text/aHR0cDovL2Jsb2cuY3Nkbi5uZXQvaHphY2NwMw==/font/5a6L5L2T/fontsize/400/fill/I0JBQkFCMA==/dissolve/70/gravity/SouthEast" alt="" /><br />
<br />
<br />
<strong>生成的JAVA代码：</strong><br />
<img src="http://img.blog.csdn.net/20130821225320546?watermark/2/text/aHR0cDovL2Jsb2cuY3Nkbi5uZXQvaHphY2NwMw==/font/5a6L5L2T/fontsize/400/fill/I0JBQkFCMA==/dissolve/70/gravity/SouthEast" alt="" /><br />
<br />
<br />